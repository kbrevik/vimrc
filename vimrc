vim9script

var base16colorspace = 256

# Optional packages
# packadd vim-css-color
packadd vim-chatgpt
packadd base16-vim
packadd vim-copilot

# Set modern vim
set nocompatible
filetype plugin indent on
syntax on

# Set default sql file
#let g:sql_type_default = 'pqsql'

# Undofile
set undofile
set undodir=$HOME/.vim/undodir

# Swap directory
set directory=$HOME/.vim/swp//

# Enable plugins
runtime ftplugin/man.vim

# Improve vim looks
colorscheme Tomorrow-Night
set guifont=Menlo\ Regular:h16
set number
set textwidth=120
set wildmode=list:longest # Setup tab completion to work like in a shell.
set ignorecase # Case-insensitive search
set smartcase # Case sensitive search if expression contains capital letter.
set title #Show teminal title
set scrolloff=3 # Show 3 lines around cursor,
set hidden
set history=100
set nowrap
set tabstop=2
set shiftwidth=2
set expandtab
set smartindent
set autoindent
set hlsearch
set showmatch
set fillchars+=vert:\| # Set | as vertical splitter

hi VertSplit ctermfg=239 ctermbg=NONE term=NONE # Customise vertical split
hi TabLine ctermbg=250 ctermfg=235
hi TabLineSel ctermfg=222 
hi TabLineFill ctermfg=235

# Set color of the line numbers
# highlight LineNr ctermfg=grey
# Colors for Terminal mode
# highlight Terminal ctermbg=black
# highlight Terminal rubyConstant ctermfg=LightRed

# def CustomizeTerminalColors()
#   highlight Term ctermbg=none ctermfg=none
#   highlight TermCursor ctermfg=black ctermbg=white
#   highlight TermCursorNC ctermfg=white ctermbg=black
#   highlight rubyConstant ctermfg=LightYellow guifg=#FFFFAF
# enddef

# augroup CustomizeTerminalColors
#   autocmd!
#   autocmd TermEnter * call CustomizeTerminalColors()
#   autocmd BufWinEnter term://* call CustomizeTerminalColors()
# augroup END

# Colors for Terminal mode
highlight Terminal ctermbg=black

# Netrw options
# g:netrw_browse_split = 4
# g:netrw_winsize = 20

# Set indentation on embedded css and js in html
g:html_indent_script1 = "inc"
g:html_indent_style1 = "inc"

# Search down into subfolders
set path+=**

# Display all matching files when we tab complete
set wildmenu

# Set SnipMate parser to 1
g:snipMate = { 'snippet_version': 1 }

# Tmux compatibility
set background=dark
set t_Co=256

#Fuzzy Find
set rtp+=/opt/homebrew/opt/fzf # Apple Silicon
#set rtp+=/usr/local/opt/fzf # x86 Intel

#highlight Cursor guifg=black guibg=white
#highlight iCursor guifg=steelblue guibg=white
#set guicursor=n-v-c:block-Cursor

# Autoclose tags
#inoremap ( ()<Left>
#inoremap [ []<Left>
#inoremap { { }<Left>
#inoremap " ""<Left>

# My key bindings
# My leader
map <Space> <Leader>

#map <Leader>r gg=G<C-O><C-O><CR>
# map <Leader>r gg=G`^zz # Indent every line in current buffer
map <Leader>r mzgg=G'zzz # Indent every line in current buffer
map <Leader><Space> :nohlsearch<CR>
noremap <leader>w :write<CR>

# Switching buffers
noremap <Leader>b :bnext<CR>
noremap <Leader>B :bprevious<CR>

# Close buffer
noremap <C-x> :bd<CR>

# Fuzzy find
noremap <Leader>f :FZF<CR>

# Quickfix navigation
map <Leader>n :cnext<CR>
map <Leader>N :cprevious<CR>

# Run Rails command
map <Leader>R :Rails<CR>

# New esc
inoremap jk <ESC>

# Insert new line below in normal mode
#nmap oo o<Esc>k
# Insert new line above in normal mode
#nmap OO O<Esc>j
# Empower Left and right
#noremap H ^

# Map goto tag
map <C-s> <C-]>

# Switch panes using ctrl-h,j,k,l
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

# Map open file unde cursor in split view
# Vertical
# or noremap <Leader>V <C-w> vgf
noremap <Leader>V <C-w>f <C-w>L

map <F11> :make % <CR>

# Check ruby syntax
map <leader>s :!/Users/kjetil/.asdf/shims/ruby -c % <CR>

# Git shortcuts (requires fugitive.vim))
noremap <Leader>gw :Gwrite<CR>
noremap <Leader>gs :Git status<CR>
noremap <Leader>gc :Git commit<CR>
noremap <Leader>gi :Git diff<CR>
noremap <Leader>gis :Git diff --staged<CR>
noremap <Leader>gm :GMove %<CR>
noremap <Leader>gl :Git log %<CR>
noremap <Leader>gf :GitFiles<CR>

# Copilot shortcuts
noremap <Leader>cc :Copilot<CR>
noremap <Leader>cp :Copilot panel<CR>

# Use w!! do sudo write current buffer
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

# My autocommands
# Check syntax in ruby files
autocmd FileType ruby compiler ruby
# Strip trailing white space before write
# autocmd BufWritePre * :%s/\s\+$//e

# Commands
command! MakeTags !ctags .

# Edit and load vimrc
nnoremap <Leader>ve :tabedit ~/.vim/vimrc<CR>
nnoremap <Leader>vl :source ~/.vim/vimrc<CR>

# My custom functions
def ExecuteFile()
  if &filetype == 'ruby'
    exe 'ruby %'
    # else
    #   return
  endif
enddef

# Testing
g:['test#strategy'] = "dispatch"

if filereadable('Dockerfile')
  g:['test#strategy'] = "vimterminal"
  g:['test#ruby#rspec#executable'] = 'docker compose exec app bundle exec rspec'
endif

nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>

# Vimwiki
g:vimwiki_list = [{'path': '~/Wiki/source/', 'path_html': '~/Wiki/public/'}]

