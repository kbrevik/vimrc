# My Vim configuration

This is my vim setup.
It uses packages for plugins so it requires Vim 8 or later.

Download my setup
```
git clone 'https://gitlab.com/kbrevik/vimrc' $HOME/.vim
```

Download submodules
```
git submodule update --init --recursive
```

Update submodules
```
git submodule update --recursive --remote
```

Add new plugins
```
git submodule add [URL].git pack/plugins/start/[LOCAL_NAME]
```
